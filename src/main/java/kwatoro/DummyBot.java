package kwatoro;

import kwatoro.message.Action;
import kwatoro.message.Action.Throttle;
import kwatoro.message.CarPosition;
import kwatoro.message.CarPositions;

import java.util.List;

public class DummyBot extends Bot {

    private final double throttle;

    public DummyBot(double throttle) {
        this.throttle = throttle;
    }

    @Override
    public Action getAction(CarPositions carPositions) {
        return new Throttle(throttle);
    }

    public static void main(String[] args) {
        Runner.run(MapName.USA, new DummyBot(1), new DummyBot(0.96), new DummyBot(0.92));
    }
}
