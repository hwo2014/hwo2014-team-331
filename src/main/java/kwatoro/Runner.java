package kwatoro;

import java.util.Random;

import kwatoro.message.CarId;
import kwatoro.message.CarPositions;
import kwatoro.message.Communicator;
import kwatoro.message.CreateRace;
import kwatoro.message.GameEnd;
import kwatoro.message.GameInit;
import kwatoro.message.Join;
import kwatoro.message.LapFinished;
import kwatoro.message.Wrappable;
import kwatoro.message.Wrapper;

public class Runner {

  private static final Random RANDOM = new Random();

  public static final String DEFAULT_HOST = "webber.helloworldopen.com";
  public static final int DEFAULT_PORT = 8091;
  public static final String DEFAULT_BOT_NAME = "Kwatoro";
  public static final String DEFAULT_BOT_KEY = "Kxt+RbY9VQdLvw";

  public static void run(Bot bot) {
    run(bot, DEFAULT_HOST, DEFAULT_PORT, DEFAULT_BOT_NAME, DEFAULT_BOT_KEY);
  }

  public static void run(
      Bot bot,
      String host,
      int port,
      String botName,
      String botKey) {
    Utils.LOGGER.info("Connecting to " +
        host + ":" +
        port + " as " +
        botName + "/" +
        botKey);

    Communicator communicator = new Communicator(host, port);
    communicator.send(new Join(botName, botKey));
    run(bot, communicator);
  }

  private static void run(Bot bot, Communicator communicator) {
    while (true) {
      Wrapper wrapper = communicator.get();
      if (wrapper == null) {
        Utils.LOGGER.severe("It broke :(");
        return;
      }

      Wrappable response = dispatch(wrapper, bot);

      communicator.send(response);
    }
  }

  public static void main(String[] args) {
    String host = args[0];
    int port = Integer.parseInt(args[1]);

    run(new DummyBot(0.59), host, port, DEFAULT_BOT_NAME, DEFAULT_BOT_KEY);
  }

  private static Wrappable dispatch(Wrapper wrapper, Bot bot) {
    Wrappable toSend = null;

    switch (wrapper.msgType) {
      case "gameEnd":
        bot.gameEnd(wrapper.unwrap(GameEnd.class));
        break;
      case "gameStart":
        bot.gameStart();
        break;
      case "gameInit":
        bot.gameInit(wrapper.unwrap(GameInit.class));
        break;
      case "joinRace":
      case "join":
      case "createRace":
        // ignore
        break;
      case "yourCar":
        bot.yourCar(wrapper.unwrap(CarId.class));
        break;
      case "tournamentEnd":
        bot.tournamentEnd();
        break;
      case "crash":
        bot.crash(wrapper.unwrap(CarId.class));
        break;
      case "spawn":
        bot.spawn(wrapper.unwrap(CarId.class));
        break;
      case "lapFinished":
        bot.lapFinished(wrapper.unwrap(LapFinished.class));
        break;
      case "carPositions":
        CarPositions carPositions = wrapper.unwrap(CarPositions.class);
        toSend = bot.getAction(carPositions);
        break;
      default:
        Utils.LOGGER.severe("Unrecognized type " + wrapper.msgType + " " + wrapper.data);
    }

    return toSend;
  }


  public static void run(MapName mapName, Bot... bots) {
    String name = mapName.name;
    String password = randomString();

    for (Bot bot : bots) {
      Communicator communicator = new Communicator(DEFAULT_HOST, DEFAULT_PORT);
      communicator.send(
          new CreateRace(bot, DEFAULT_BOT_KEY, name,
              password, bots.length)
      );
      new Thread(() -> {
        Utils.LOGGER.info("run " + bot);
        run(bot, communicator);
      }).start();
    }
  }

  private static String randomString() {
    int length = 12;
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; ++i) {
      sb.append((char) (RANDOM.nextInt(26) + 'a'));
    }
    return sb.toString();
  }
}
