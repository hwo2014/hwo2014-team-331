package kwatoro.message;

import kwatoro.Bot;

public class CreateRace implements Wrappable {
    final BotId botId;
    final String trackName;
    final String password;
    int carCount;

    public static class BotId {
        BotId(String name, String key) {
            this.name = name;
            this.key = key;
        }

        final String name;
        final String key;
    }
    public CreateRace(Bot bot, String key, String trackName, String password, int carCount) {
        botId = new BotId(Integer.toString(bot.hashCode(), 16), key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    public String getType() {
        return "joinRace";
    }

    @Override
    public Object getData() {
        return this;
    }
}
