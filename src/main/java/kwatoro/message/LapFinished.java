package kwatoro.message;

public class LapFinished {

  public CarId car;
  public LapResult lapTime;
  public RaceResult raceTime;
  public Ranking ranking;

  public static class Ranking {

    public int overall;
    public int fastestLap;
  }
}
