package kwatoro.message;

public class Join implements Wrappable {

  private final String name;
  private final String key;

  @Override
  public Object getData() {
    return this;
  }

  @Override
  public String getType() {
    return "join";
  }

  public Join(String name, String key) {
    this.name = name;
    this.key = key;
  }
}
