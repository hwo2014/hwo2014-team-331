package kwatoro.message;

public class CarId {

  public final String name;
  public final String color;

  public CarId(String name, String color) {
    this.name = name;
    this.color = color;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    CarId carId = (CarId) obj;

    if (color != null ? !color.equals(carId.color) : carId.color != null) {
      return false;
    } else {
      return name != null ? name.equals(carId.name) : carId.name == null;
    }
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (color != null ? color.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CarId{" +
        "name='" + name + '\'' +
        ", color='" + color + '\'' +
        '}';
  }
}
