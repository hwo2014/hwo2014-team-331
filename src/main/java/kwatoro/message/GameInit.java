package kwatoro.message;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GameInit {

  public Race race;

  public static class Race {

    public Track track;
    public List<Car> cars;
    public RaceSession raceSession;

    public static class Track {

      public String id;
      public String name;
      public List<Piece> pieces;
      public List<Lane> lanes;
      public StartingPoint startingPoint;

      public static class Piece {

        public Double length;
        @SerializedName("switch")
        public Boolean isSwitch;
        public Double angle;
        public Integer radius;

        public double getLength(Lane lane) {
          if (length != null) {
            return length;
          }
          return Math.abs(getEffectiveRadius(lane) * angle / 180 * Math.PI);
        }

        public double getEffectiveRadius(Lane lane) {
          if (radius == null) {
            return 0;
          } else {
            double effectiveRadius;
            if (angle < 0) {
              effectiveRadius = -radius;
            } else {
              effectiveRadius = radius;
            }
            effectiveRadius -= lane.distanceFromCenter;
            return effectiveRadius;
          }
        }
      }

      public static class Lane {

        public double distanceFromCenter;
        public int index;
      }

      public static class StartingPoint {

        public class Point {

          public double x;
          public double y;
        }

        public Point position;
        public double angle;
      }
    }

    public static class Car {

      public CarId id;
      public Dimensions dimensions;

      public static class Dimensions {

        public double length;
        public double width;
        public double guideFlagPosition;
      }
    }

    public static class RaceSession {

      public int laps;
      public int maxLapTimeMs;
      public boolean quickRace;
      public int durationMs;
    }
  }
}
