package kwatoro.message;

import java.util.ArrayList;

public class CarPositions extends ArrayList<CarPosition> {

  public CarPosition get(CarId carId) {
    for (CarPosition position : this) {
      if (position.id.equals(carId)) {
        return position;
      }
    }

    throw new IllegalArgumentException();
  }

}
