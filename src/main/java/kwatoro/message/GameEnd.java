package kwatoro.message;

import java.util.List;

public class GameEnd {

  public List<GameResult> results;
  public List<BestLap> bestLaps;

  public static class GameResult {

    public CarId car;
    public RaceResult result;
  }

  public static class BestLap {

    public CarId car;
    public LapResult result;
  }

}
