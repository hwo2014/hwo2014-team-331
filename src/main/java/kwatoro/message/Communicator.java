package kwatoro.message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

import kwatoro.Utils;

/**
 * Communicates with the server.
 */
public class Communicator {

  private final PrintWriter writer;
  private final BufferedReader reader;
  private final Socket socket;

  public Communicator(String host, int port) {
    try {
      socket = new Socket(host, port);

      writer = new PrintWriter(
          new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

      reader = new BufferedReader(
          new InputStreamReader(socket.getInputStream(), "utf-8"));

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void send(Wrappable wrappable) {
    if (wrappable == null) {
      return;
    }

    String json = Wrapper.wrap(wrappable).toJson();

    Utils.LOGGER.finest("Sending " + json);

    writer.println(json);
    writer.flush();
  }

  public Wrapper get() {
    try {
      String line = reader.readLine();

      Utils.LOGGER.finest("Got " + line);

      if (line == null) {
        return null;
      }

      return Wrapper.fromJson(line);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
