package kwatoro.message;

public class Ping implements Wrappable {

  @Override
  public Object getData() {
    return null;
  }

  @Override
  public String getType() {
    return "ping";
  }
}
