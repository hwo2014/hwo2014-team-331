package kwatoro.message;

/**
 * An abstract class for a message that can be sent.
 */
public interface Wrappable {

  String getType();

  Object getData();
}
