package kwatoro.message;

public abstract class Action implements Wrappable {

    public static class Throttle extends Action {

        private final double throttle;

        public Throttle(double throttle) {
            this.throttle = throttle;
        }

        @Override
        public String getType() {
            return "throttle";
        }

        @Override
        public Object getData() {
            return throttle;
        }
    }

    public static class SwitchLine extends Action {

        public enum Direction {
            LEFT("Left"), RIGHT("Right");

            public final String representation;

            Direction(String representation) {
                this.representation = representation;
            }
        }

        private final Direction direction;

        public SwitchLine(Direction direction) {
            this.direction = direction;
        }

        @Override
        public String getType() {
            return "switchLine";
        }

        @Override
        public Object getData() {
            return direction.representation;
        }
    }

    public static class Turbo extends Action {

        @Override
        public String getType() {
            return "turbo";
        }

        @Override
        public Object getData() {
            return "Run, kwatoro, run";
        }
    }

}
