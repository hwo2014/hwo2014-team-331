package kwatoro.message;

import com.google.gson.Gson;

/**
 * A wrapper class for Gson parsed message.
 */
public class Wrapper {

  private static final Gson GSON = new Gson();

  public static Wrapper fromJson(String json) {
    return GSON.fromJson(json, Wrapper.class);
  }

  public static Wrapper wrap(Wrappable wrappable) {
    return new Wrapper(wrappable.getType(), wrappable.getData());
  }

  public String msgType;
  public Object data;
  public String gameId;
  public Integer gameTick;

  private Wrapper(String msgType, Object data) {
    this.msgType = msgType;
    this.data = data;
  }

  public String toJson() {
    return GSON.toJson(this);
  }

  public <T> T unwrap(Class<T> clazz) {
    String dataJson = GSON.toJson(data);
    return GSON.fromJson(dataJson, clazz);
  }
}
