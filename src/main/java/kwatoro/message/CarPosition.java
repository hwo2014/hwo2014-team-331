package kwatoro.message;

import com.google.gson.annotations.SerializedName;

public class CarPosition {

  public CarId id;
  public double angle;
  public PiecePosition piecePosition;

  public static class PiecePosition {

    public int pieceIndex;
    public double inPieceDistance;
    @SerializedName("lane")
    public LaneSwitchInfo laneSwitchInfo;
    public int lap;

    public static class LaneSwitchInfo {

      public int startLaneIndex;
      public int endLaneIndex;

      @Override
      public String toString() {
        return "Lane{" +
            "startLaneIndex=" + startLaneIndex +
            ", endLaneIndex=" + endLaneIndex +
            '}';
      }
    }

    @Override
    public String toString() {
      return "PiecePosition{" +
          "pieceIndex=" + pieceIndex +
          ", inPieceDistance=" + inPieceDistance +
          ", lane=" + laneSwitchInfo +
          ", lap=" + lap +
          '}';
    }
  }
}
