package kwatoro;

import java.util.ArrayList;
import java.util.List;

import kwatoro.message.Action;
import kwatoro.message.Action.Throttle;
import kwatoro.message.CarId;
import kwatoro.message.CarPosition;
import kwatoro.message.CarPositions;
import kwatoro.message.GameInit;
import kwatoro.message.GameInit.Race.Track;

public class CleverBot extends Bot {

  private static final int ITERATIONS = 50;
  private static final double ANGLE_LIMIT = 57;
  private static final int LOOK_AHEAD = 50;
  private Track track;
  private List<CarPosition> qualLog = new ArrayList<>();
  private List<CarPosition> log = new ArrayList<>();
  private boolean qual = false;

  @Override
  public void gameInit(GameInit gameInit) {
    track = gameInit.race.track;

    qual = gameInit.race.raceSession.durationMs != 0;

    state = new State(
        track,
        0,
        0.0,
        0.0,
        0.0,
        0.0,
        0);
  }

  private CarId myCar;

  @Override
  public void crash(CarId carId) {
    super.crash(carId);
    Utils.LOGGER.warning("crash, predicted angle: " + state.angle);
  }

  @Override
  public void yourCar(CarId carId) {
    myCar = carId;
  }

  @Override
  public void spawn(CarId carId) {
    super.spawn(carId);
    state = new State(state.track, state.pieceIndex, state.distanceInPiece, 0, 0, 0,
        state.laneIndex);
  }

  State state;

  boolean willFail(State state) {
    double maxAbsAngle = 0;
    maxAbsAngle = Math.max(maxAbsAngle, Math.abs(state.angle));
    for (int i = 0; i < LOOK_AHEAD; ++i) {
      state = state.updateTick(0.0);
      maxAbsAngle = Math.max(maxAbsAngle, Math.abs(state.angle));
    }
    return maxAbsAngle > ANGLE_LIMIT;
  }

  @Override
  public Action getAction(CarPositions carPositions) {
    CarPosition position = carPositions.get(myCar);

    //assumes that willFail is monotone
    double minThrottle = 0.0;
    double maxThrottle = 1.0;

    if (willFail(state.updateTick(1.0))) {
      for (int iteration = 0; iteration < ITERATIONS; ++iteration) {
        double midThrottle = (minThrottle + maxThrottle) / 2;

        State possibleNextState = state.updateTick(midThrottle);
        if (willFail(possibleNextState)) {
          maxThrottle = midThrottle;
        } else {
          minThrottle = midThrottle;
        }
      }
    } else {
      minThrottle = 1.0;
    }


    Utils.LOGGER.info(position.piecePosition.pieceIndex
        + " " + position.piecePosition.lap
        + " " + minThrottle
        + " " + state.angle
        + " " + position.angle);
    state = state.updateTick(minThrottle);

    return new Throttle(minThrottle);
  }

  public static void main(String[] args) {
    Runner.run(MapName.KEIMOLA, new CleverBot());
  }
}
