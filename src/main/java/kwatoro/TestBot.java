package kwatoro;

import java.util.List;

import kwatoro.message.Action;
import kwatoro.message.Action.Throttle;
import kwatoro.message.CarPosition;
import kwatoro.message.CarPositions;
import kwatoro.message.GameInit;
import kwatoro.message.GameInit.Race.Track;
import kwatoro.message.GameInit.Race.Track.Piece;

public class TestBot extends Bot {

  private double speed = 0.0;

  private Track track;

  @Override
  public void gameInit(GameInit gameInit) {
    track = gameInit.race.track;
  }

  double t = 0.0;

  @Override
  public Action getAction(CarPositions carPositions) {
    CarPosition carPosition = carPositions.get(0);

    int curIndex = carPosition.piecePosition.pieceIndex;
    Piece curPiece = track.pieces.get(curIndex);

    double speed = carPosition.piecePosition.inPieceDistance - t;
//
//    Utils.LOGGER.info(carPosition.piecePosition.inPieceDistance + "");
//    Utils.LOGGER.info(carPosition.angle + "," + speed + "," + curPiece.radius
//        + "," + curPiece.angle + "," + curIndex);

    t = carPosition.piecePosition.inPieceDistance;

    updateSpeed(1.0);

    if (con) {
      return new Throttle(fix);
    }

    if (speed < 6.5) {
      return new Throttle(0.7);
    } else {
      con = true;
      fix = speed / 10;
      System.out.println(fix);
      return new Throttle(fix);
    }
  }

  boolean con;
  double fix;


  private void updateSpeed(double throttle) {
    speed = 0.98 * speed + throttle * 0.2;
  }

  public static void main(String[] args) {
    Runner.run(MapName.KEIMOLA, new TestBot());
  }

}
