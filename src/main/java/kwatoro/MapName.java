package kwatoro;

public enum MapName {

  KEIMOLA("keimola"), GERMANY("germany"), USA("usa"), FRANCE("france");

  public final String name;

  MapName(String name) {
    this.name = name;
  }
}
