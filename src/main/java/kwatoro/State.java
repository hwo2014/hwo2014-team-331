package kwatoro;

import com.google.common.annotations.VisibleForTesting;

import kwatoro.message.GameInit.Race.Track;
import kwatoro.message.GameInit.Race.Track.Lane;
import kwatoro.message.GameInit.Race.Track.Piece;

public class State {


    private static final double C0 = -5.26802579941402e-02;
    private static final double C2_1 = -9.02644653517624e-03;
    private static final double C2_2 = -1.74771050529248e-02;
    private static final double C4_1 = 300 * Math.sqrt(2.0);
    private static final double C4_2 = 240;
    private static final double INERTION_FACTOR = 0.98;
    private static final double ENGINE_POWER = 0.2;

    public final Track track;
    public final int pieceIndex;
    public final double distanceInPiece;
    public final double angle;
    public final double speed;
    public final double angleSpeed;
    public final int laneIndex;
    public final Lane lane;
    public final Piece piece;
    public final double radius;

    public State(
            Track track,
            int pieceIndex,
            double distanceInPiece,
            double angle,
            double speed,
            double angleSpeed,
            int laneIndex) {
        this.track = track;
        this.pieceIndex = pieceIndex;
        this.distanceInPiece = distanceInPiece;
        this.angle = angle;
        this.speed = speed;
        this.angleSpeed = angleSpeed;
        this.laneIndex = laneIndex;

        lane = track.lanes.get(laneIndex);
        piece = track.pieces.get(pieceIndex);
        radius = piece.getEffectiveRadius(lane);
    }

    public State updateTick(double throttle) {
        return new State(
                track,
                pieceIndex,
                distanceInPiece,
                angle,
                speed * INERTION_FACTOR + ENGINE_POWER * throttle,
                angleSpeed,
                laneIndex).moveConstantSpeed(1);
    }

    @VisibleForTesting
    State moveConstantSpeed(double time) {
        double nextDistanceInPiece = distanceInPiece + speed * time;


        if (nextDistanceInPiece > piece.getLength(lane)) {
            double timeToChange = (piece.getLength(lane) - distanceInPiece) / speed;

            State result = moveConstantPieceSpeed(timeToChange);
            result = new State(
                    track,
                    (pieceIndex + 1) % track.pieces.size(),
                    0,
                    result.angle,
                    speed,
                    result.angleSpeed,
                    laneIndex);

            return result.moveConstantSpeed(time - timeToChange);
        } else {
            return moveConstantPieceSpeed(time);
        }
    }

    @VisibleForTesting
    State moveConstantPieceSpeed(double time) {
        double C4 = 0;

        if (radius != 0) {
            C4 = Math.max(C4_1 * speed / Math.sqrt(Math.abs(radius)) - C4_2, 0)
                    * Math.signum(radius);
        }

        double C1 = angle - C4;
        double C2 = Math.abs(C2_1 + C2_2 * speed);
        double C3 = (angleSpeed - C0 * C1) / C2;

        Utils.LOGGER.finest("C: " + C0 + " " + C1 + " " + C2 + " " + C3 + " " + C4);

        double nextAngle =
                Math.exp(C0 * time)
                        * (C1 * Math.cos(C2 * time)
                        + C3 * Math.sin(C2 * time))
                        + C4;
        double nextAngleSpeed =
                Math.exp(C0 * time)
                        * (Math.sin(C2 * time) * (C0 * C3 - C1 * C2)
                        + Math.cos(C2 * time) * (C0 * C1 + C2 * C3));

        return new State(
                track,
                pieceIndex,
                distanceInPiece + speed * time,
                nextAngle,
                speed,
                nextAngleSpeed,
                laneIndex);
    }

    @Override
    public String toString() {
        return "State{" +
                "track=" + track +
                ", pieceIndex=" + pieceIndex +
                ", distanceInPiece=" + distanceInPiece +
                ", angle=" + angle +
                ", speed=" + speed +
                ", angleSpeed=" + angleSpeed +
                ", laneIndex=" + laneIndex +
                ", lane=" + lane +
                ", piece=" + piece +
                ", radius=" + radius +
                '}';
    }
}
