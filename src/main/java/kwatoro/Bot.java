package kwatoro;

import kwatoro.message.Action;
import kwatoro.message.CarId;
import kwatoro.message.CarPosition;
import kwatoro.message.CarPositions;
import kwatoro.message.GameEnd;
import kwatoro.message.GameInit;
import kwatoro.message.LapFinished;

import java.util.List;
import java.util.logging.Logger;

public abstract class Bot {

  private static final Logger LOGGER = Logger.getLogger(Bot.class.getName());

  public abstract Action getAction(CarPositions carPositions);

  public void yourCar(CarId carId) {
    LOGGER.finer("yourCar " + carId);
  }

  public void gameInit(GameInit gameInit) {
    LOGGER.finer("gameInit " + gameInit);
  }

  public void gameEnd(GameEnd gameEnd) {
    LOGGER.finer("gameEnd " + gameEnd);
  }

  public void gameStart() {
    LOGGER.finer("gameStart");
  }

  public void tournamentEnd() {
    LOGGER.finer("tournamentEnd");
  }

  public void crash(CarId carId) {
    LOGGER.warning("crash " + carId);
  }

  public void spawn(CarId carId) {
    LOGGER.finer("spawn " + carId);
  }

  public void lapFinished(LapFinished lapFinished) {
    LOGGER.info("lapFinished " + lapFinished);
  }
}
