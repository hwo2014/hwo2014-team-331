package kwatoro;

import java.util.List;

import kwatoro.message.Action;
import kwatoro.message.CarId;
import kwatoro.message.CarPosition;
import kwatoro.message.CarPositions;

public class AngleBot extends Bot {

  public static final double EPS = 0.1;
  private String color;
  int lastThrottle = 10;

  @Override
  public void yourCar(CarId carId) {
    color = carId.color;
  }

  @Override
  public Action getAction(CarPositions carPositions) {
    CarPosition position = new CarPosition();
    for (CarPosition carPosition : carPositions) {
      if (carPosition.id.color.equals(color)) {
        position = carPosition;
      }
    }
    double angle = Math.abs(position.angle);
    if (angle < EPS) {
      lastThrottle = 10;
    } else if (angle < 1) {
      lastThrottle += 1;
    } else if (angle > 20) {
      lastThrottle = 1;
    } else if (angle > 5) {
      lastThrottle -= 2;
    } else if (angle > 3) {
      lastThrottle -= 1;
    }

    if (lastThrottle < 1) {
      lastThrottle = 1;
    }
    if (lastThrottle > 10) {
      lastThrottle = 10;
    }
    return new Action.Throttle(lastThrottle / 10.0);
  }

  public static void main(String[] args) {
    Runner.run(new AngleBot());
  }
}
