package kwatoro;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Utils {

  public static final Logger LOGGER = getLogger();

  private static Logger getLogger() {
    Logger logger = Logger.getLogger("kwatoro");

    logger.setLevel(Level.INFO);

    DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss");
    Calendar cal = Calendar.getInstance();
    String now = dateFormat.format(cal.getTime());

    try {
      FileHandler fh = new FileHandler("logs/" + now + ".log");
      logger.addHandler(fh);
      SimpleFormatter formatter = new SimpleFormatter();
      fh.setFormatter(formatter);
    } catch (SecurityException | IOException e) {
      throw new RuntimeException(e);
    }

    return logger;
  }

}
