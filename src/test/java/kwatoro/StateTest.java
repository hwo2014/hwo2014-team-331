package kwatoro;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import kwatoro.message.GameInit.Race.Track;
import kwatoro.message.GameInit.Race.Track.Lane;
import kwatoro.message.GameInit.Race.Track.Piece;

public class StateTest {

  @Test
  public void testMove() {
    Track track = new Track();

    Piece piece = new Piece();
    piece.length = 10.0;
    track.pieces = new ArrayList<>();
    track.pieces.add(piece);

    Lane lane = new Lane();
    track.lanes = new ArrayList<>();
    track.lanes.add(lane);

    State state = new State(
        track,
        0,
        0,
        0,
        5,
        0,
        0);

    Assert.assertEquals(5, state.moveConstantPieceSpeed(1.0).distanceInPiece, 1e-9);

    Assert.assertEquals(3, state.moveConstantSpeed(13.0 / 5).distanceInPiece, 1e-9);
  }
}
