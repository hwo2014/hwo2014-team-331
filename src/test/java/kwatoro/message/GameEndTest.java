package kwatoro.message;

import org.junit.Assert;
import org.junit.Test;

public class GameEndTest {

    @Test
    public void weakTest() {
        String json = "{\"msgType\": \"gameEnd\", \"data\": {\n"
                + "  \"results\": [\n"
                + "    {\n"
                + "      \"car\": {\n"
                + "        \"name\": \"Schumacher\",\n"
                + "        \"color\": \"red\"\n"
                + "      },\n"
                + "      \"result\": {\n"
                + "        \"laps\": 3,\n"
                + "        \"ticks\": 9999,\n"
                + "        \"millis\": 45245\n"
                + "      }\n"
                + "    },\n"
                + "    {\n"
                + "      \"car\": {\n"
                + "        \"name\": \"Rosberg\",\n"
                + "        \"color\": \"blue\"\n"
                + "      },\n"
                + "      \"result\": {}\n"
                + "    }\n"
                + "  ],\n"
                + "  \"bestLaps\": [\n"
                + "    {\n"
                + "      \"car\": {\n"
                + "        \"name\": \"Schumacher\",\n"
                + "        \"color\": \"red\"\n"
                + "      },\n"
                + "      \"result\": {\n"
                + "        \"lap\": 2,\n"
                + "        \"ticks\": 3333,\n"
                + "        \"millis\": 20000\n"
                + "      }\n"
                + "    },\n"
                + "    {\n"
                + "      \"car\": {\n"
                + "        \"name\": \"Rosberg\",\n"
                + "        \"color\": \"blue\"\n"
                + "      },\n"
                + "      \"result\": {}\n"
                + "    }\n"
                + "  ]\n"
                + "}}";

        GameEnd gameEnd = Wrapper.fromJson(json).unwrap(GameEnd.class);

        Assert.assertEquals("red", gameEnd.bestLaps.get(0).car.color);
        Assert.assertEquals(3, gameEnd.results.get(0).result.laps);
        Assert.assertEquals(2, gameEnd.bestLaps.get(0).result.lap);
        Assert.assertEquals("Schumacher", gameEnd.results.get(0).car.name);
    }
}
